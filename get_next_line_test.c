#include "get_next_line.h"
#include <stdio.h>
#include <fcntl.h>

/*
**  char c1 = '\0';
**  char *c2;
**  char **c;
**  c2 = &c1;
**  c = &c2;
*/

void test_signe_char_eol(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");  
	fd = open("./fd_test_signe_char_eol.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) == 1)
	{
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
	}
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
    {
		printf("ret = %d | test_empty_file succeed\n", ret);
    }
	else
    {
		printf("ret = %d | test_empty_file failed\n", ret);
    }
	close(fd);
	printf("--------------------------------------------\n");
}

void test_single_char_eol(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");
	//fd = open("./gnlkiller/tests/63_line_nl", O_RDONLY);
	fd = open("./fd_test_single_char.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) > 0)
	{
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
	}
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
    {
		printf("ret = %d | test_single_char succeed\n", ret);
    }
	else
    {
		printf("ret = %d | test_single_char failed\n", ret);
    }
	close(fd);
	printf("--------------------------------------------\n");
}

void test_short_line(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");
	//fd = open("./gnlkiller/tests/64_line", O_RDONLY);
	fd = open("./fd_test_short_line.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) > 0)
	{
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
	}
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
    {
		printf("ret = %d | test_short_line succeed\n", ret);
    }
	else
    {
		printf("ret = %d | test_short_line failed\n", ret);
    }
	close(fd);
	printf("--------------------------------------------\n");
}

void test_long_line(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");
	//fd = open("./gnlkiller/tests/64_line_nl", O_RDONLY);
	fd = open("./fd_test_long_line.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) > 0)
	{
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
	}
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
    {
		printf("ret = %d | test_long_line succeed\n", ret);
    }
	else
    {
		printf("ret = %d | test_long_line failed\n", ret);
    }
	close(fd);
	printf("--------------------------------------------\n");
}

void test_short_bloc(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");
	//fd = open("./gnlkiller/tests/65_line", O_RDONLY);
	fd = open("./fd_test_short_bloc.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) > 0)
    {
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
    }
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
    {
		printf("ret = %d | test_short_bloc succeed\n", ret);
    }
	else
    {
		printf("ret = %d | test_short_bloc failed\n", ret);
    }
	close(fd);
	printf("--------------------------------------------\n");
}

void test_long_bloc(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");
	//fd = open("./gnlkiller/tests/65_line_nl", O_RDONLY);
	fd = open("./fd_test_long_bloc.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) > 0)
    {
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
    }
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
    {
		printf("ret = %d | test_long_bloc succeed\n", ret);
    }
	else
    {
		printf("ret = %d | test_long_bloc failed\n", ret);
    }
	close(fd);
	printf("--------------------------------------------\n");
}

void test_short_chapter(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");
	//fd = open("./gnlkiller/tests/large_file.txt", O_RDONLY);
	fd = open("./fd_test_short_chapter.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) > 0)
    {
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
    }
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
    {
		printf("ret = %d | test_short_chapter succeed\n", ret);
    }
	else
    {
		printf("ret = %d | test_short_chapter failed\n", ret);
    }
	close(fd);
	printf("--------------------------------------------\n");
}

void test_long_chapter(int (*f)(int, char**))
{
	int ret;
	int fd;
	char *c;
	
	printf("--------------------------------------------\n");
	//fd = open("./gnlkiller/tests/lorem_ipsum", O_RDONLY);
	fd = open("./fd_test_long_chapter.txt", O_RDONLY);
	while ((ret = (*f)(fd, &c)) > 0)
	{
		printf("ret = %d | line = %s\n", ret, c);
		free(c);
	}
	printf("ret = %d | line = %s\n", ret, c);
	free(c);
	if (ret == 0)
	{
		printf("ret = %d | test_long_chapter succeed\n", ret);
	}
	else
	{
		printf("ret = %d | test_long_chapter failed\n", ret);
	}
	close(fd);
	printf("--------------------------------------------\n");
}

void test_multi_fd(int (*f)(int, char**))
{
	int ret;
	int fd1;
	int fd2;
	int fd3;
	int fd4;
	int fd5;
	char *c;

	printf("--------------------------------------------\n");
	fd1 = open("./fd_test_long_chapter.txt", O_RDONLY);
	fd2 = open("./fd_test_short_chapter.txt", O_RDONLY);
	fd3 = open("./fd_test_long_bloc.txt", O_RDONLY);
	fd4 = open("./fd_test_short_bloc.txt", O_RDONLY);
	fd5 = open("./fd_test_long_line.txt", O_RDONLY);
	ret = (*f)(fd1, &c);
	printf("fd 1 | | line 1 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd2, &c);
	printf("fd 2 | | line 1 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd1, &c);
	printf("fd 1 | | line 2 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd3, &c);
	printf("fd 3 | | line 1 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd4, &c);
	printf("fd 4 | | line 1 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd5, &c);
	printf("fd 5 | | line 1 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd3, &c);
	printf("fd 3 | | line 2 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd4, &c);
	printf("fd 4 | | line 2 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd1, &c);
	printf("fd 1 | | line 3 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd4, &c);
	printf("fd 4 | | line 3 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd3, &c);
	printf("fd 3 | | line 3 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd1, &c);
	printf("fd 1 | | line 4 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd5, &c);
	printf("fd 5 | | line 2 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd2, &c);
	printf("fd 2 | | line 2 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd4, &c);
	printf("fd 4 | | line 4 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd2, &c);
	printf("fd 2 | | line 3 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd1, &c);
	printf("fd 1 | | line 5 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd2, &c);
	printf("fd 2 | | line 3 | %d | %s\n", ret, c);
	free(c);
	ret = (*f)(fd5, &c);
	printf("fd 5 | | line 3 | %d | %s\n", ret, c);
	free(c);
	close(fd1);
	close(fd2);
	close(fd3);
	close(fd4);
	close(fd5);
	printf("--------------------------------------------\n");
}

int main()
{
	int (*get_next_line_ptr)(int, char**);
	
	get_next_line_ptr = &get_next_line;  
	test_signe_char_eol(get_next_line_ptr);
	test_single_char_eol(get_next_line_ptr);
	test_short_line(get_next_line_ptr);
	test_long_line(get_next_line_ptr);
	test_short_bloc(get_next_line_ptr);
	test_long_bloc(get_next_line_ptr);
	test_short_chapter(get_next_line_ptr);
	test_long_chapter(get_next_line_ptr);
	//test_multi_fd(get_next_line_ptr);
	system("leaks a.out\n");
}
